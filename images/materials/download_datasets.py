#/bin/env python3
#
# Authors:
#  - Oliver Rippel <oliver.rippel@lfb.rwth-aachen.de>
#  - Steffen Vogel <svogel2@eonerc.rwth-aachen.de>
#
# For profile: MLL

import os
import shutil
import urllib

from scipy.io import loadmat

from torchvision.datasets import Caltech101
from torchvision.datasets import MNIST
from torchvision.datasets import VOCSegmentation

def load_mnist(dpath):
    mnist_url = 'https://github.com/amplab/datascience-sp14/raw/master/lab7/mldata/mnist-original.mat'
    mnist_path = os.path.join(dpath, 'mnist-original.mat')
    response = urllib.request.urlopen(mnist_url)
    with open(mnist_path, 'wb') as f:
        content = response.read()
        f.write(content)

def load_caltech_101(dpath):
    Caltech101(dpath, download=True)

def load_mnist_torch(dpath):
    MNIST(dpath, download=True)

    if os.path.isdir(os.path.join(dpath, 'MNIST', 'raw')):
        shutil.rmtree(os.path.join(dpath, 'MNIST', 'raw'))

def load_voc(dpath):
    VOCSegmentation(root=dpath, download=True)

def remove_tars(dpath):
    for root, _, fpaths in os.walk(dpath):
        for fpath in fpaths:
            if '.tar' in fpath:
                os.remove(os.path.join(root, fpath))

def load_datasets(path):
    for load_fun in (load_caltech_101, load_mnist, load_voc, load_mnist_torch):
        load_fun(path)

    # clean_up
    remove_tars(path)

if __name__ == "__main__":
    path = os.environ.get('PATH_DATASETS')

    load_datasets(path)
