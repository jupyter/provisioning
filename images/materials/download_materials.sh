#!/bin/bash

# Environemnt variables:
#  - REPO
#  - PATH_MATERIALS

if [ -d ${PATH_MATERIALS}/.git ]; then
    git -C ${PATH_MATERIALS} pull
else
    git clone --recurse-submodules ${REPO} ${PATH_MATERIALS}
fi