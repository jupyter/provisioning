#!/bin/bash

LOGGING=${LOGGING:-false}
PREFIX=${PREFIX:-64:ff9b::/96}

if [ -z "${INSTANCE}" ]; then
    INSTANCE="k8s-$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 8)"
fi

log() {
    echo "[$(date --rfc-3339=seconds)] $*"
}

_term() {
    log "Starting shutdown"

    log "Remove Jool instance: ${INSTANCE}"
    jool -f /etc/jool/jool.conf instance remove

    log "Unloading Kernel module"
    modprobe -r jool

    exit 0
}
trap _term SIGTERM
trap _term SIGINT

log "Comparing Kernel versions"
if [ "$(modinfo -F vermagic jool | cut -f1 -d' ')" != $(uname -r) ]; then
    log "Invalid Kernel version!"
    log "  Jool was build for Kernel: $(modinfo -F vermagic jool | cut -f1 -d' ')"
    log "  We are running Kernel:     $(uname -r)"
    exit 1
fi

if [ -n "$(grep -e "^jool " /proc/modules)" ]; then
    log "Unloading previously loaded module"
    modprobe -r jool
fi

log "Loading Jool kernel module"
modprobe jool

log "Generating /etc/jool/jool.conf"
cat << EOF > /etc/jool/jool.conf
{
    "comment": "Full NAT64 configuration.",
    "instance": "${INSTANCE}",
    "framework": "netfilter",
    "global": {
        "pool6": "${PREFIX}",
        "logging-session": ${LOGGING}
    }
}
EOF

log "Remove stale Jool instances"
for I in $(jool instance display --csv --no-headers | cut -f2 -d,); do
    jool instance remove $I
done


log "Creating new Jool instance: ${INSTANCE}"
jool file handle /etc/jool/jool.conf

if [ "${LOGGING}" == "true" ]; then
    dmesg -wkP | awk -F "${INSTANCE} " "/Jool: ${INSTANCE}/{print \$2}" - &
    wait $!
else
    wait
fi
