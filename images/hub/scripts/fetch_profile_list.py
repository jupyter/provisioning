import yaml
import asyncio

from tornado.httpclient import AsyncHTTPClient

async def fetch_profile_list():
    hc = AsyncHTTPClient()
    try:
        response = await hc.fetch('http://[::1]:5000/api/v1/artifacts/generate/jupyter-profile-config.yml')
        
        return yaml.safe_load(response.body)
    except Exception as e:
        print("Error: " + str(e))

    hc.close()

    return []

# The rest is just for testing
async def load():
    pl = await fetch_profile_list()
    print(pl)

def main():
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    result = loop.run_until_complete(load())
