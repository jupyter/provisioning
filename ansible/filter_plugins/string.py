from jinja2.utils import soft_unicode

def string_repr(s):
    return repr(s)

def string_format(text, fmt, *kw):
    return soft_unicode(fmt).format(text, *kw)

def string_prefix(s, prefix):
    return prefix + s

def string_postfix(s, postfix):
    return s + postfix

def string_split(string, separator=' '):
    try:
        return string.split(separator)
    except Exception as e:
        raise errors.AnsibleFilterError('split plugin error: %s, provided string: "%s"' % str(e),str(string) )

def string_slugify(s):
    non_url_safe = ['"', '#', '$', '%', '&', '+',
                    ',', '/', ':', ';', '=', '?',
                    '@', '[', '\\', ']', '^', '`',
                    '{', '|', '}', '~', "'", "."]

    non_safe = [ c for c in s if c in non_url_safe ]
    if non_safe:
        for c in non_safe:
            s = s.replace(c, '-')

    # Strip leading, trailing and multiple whitespace, convert remaining whitespace to _
    return '-'.join(s.split())

class FilterModule(object):
    ''' Ansible filters. Python string operations.'''
    def filters(self):
        return {
            'format' : string_format,
            'prefix' : string_prefix,
            'postfix' : string_postfix,
            'slugify' : string_slugify,
            'split' : string_split,
            'repr'  : string_repr
        }
