import yaml

from ansible.module_utils._text import to_text
from ansible.parsing.yaml.dumper import AnsibleDumper

def to_nice_yaml_all(a, indent=4, *args, **kw):
    '''Make verbose, human readable yaml'''
    transformed = yaml.dump_all(a, Dumper=AnsibleDumper, indent=indent, allow_unicode=True, default_flow_style=False, **kw)
    return to_text(transformed)


class FilterModule(object):
    ''' Ansible filters. Python string operations.'''
    def filters(self):
        return {
            'to_nice_yaml_all' : to_nice_yaml_all
        }
