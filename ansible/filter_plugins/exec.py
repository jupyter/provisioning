import subprocess
from ansible.module_utils._text import to_text

def exec_subprocess(s, *args, **kwargs):
    out = subprocess.check_output(*args, input=s.encode(), **kwargs)

    return to_text(out)

class FilterModule(object):
    ''' Ansible filters. Python string operations.'''
    def filters(self):
        return {
            'exec' : exec_subprocess,
        }
