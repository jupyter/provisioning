---
apiVersion: v1
kind: ConfigMap
metadata:
  name: fluentd-forwarder
  namespace: {{ efk_namespace }}
data:
  fluentd.conf: |
    # Prometheus Exporter Plugin
    # input plugin that exports metrics
    <source>
      @type prometheus
      port 24231
    </source>
    
    # input plugin that collects metrics from MonitorAgent
    <source>
      @type prometheus_monitor
      <labels>
        host ${hostname}
      </labels>
    </source>
    
    # input plugin that collects metrics for output plugin
    <source>
      @type prometheus_output_monitor
      <labels>
        host ${hostname}
      </labels>
    </source>
    
    # input plugin that collects metrics for in_tail plugin
    <source>
      @type prometheus_tail_monitor
      <labels>
        host ${hostname}
      </labels>
    </source>

    # Ignore fluentd own events
    <match fluent.**>
      @type null
    </match>
    
    # HTTP input for the liveness and readiness probes
    <source>
      @type http
      port 9880
      bind ::
    </source>
    
    # Throw the healthcheck to the standard output instead of forwarding it
    <match fluentd.healthcheck>
      @type stdout
    </match>
    
    # Get the logs from the containers running in the node
    <source>
      @type tail
      path /var/log/containers/*.log
      # exclude Fluentd logs
      exclude_path /var/log/containers/*fluentd*.log
      pos_file /opt/bitnami/fluentd/logs/buffers/fluentd-docker.pos
      tag kubernetes.*
      read_from_head true
      <parse>
        @type json
        time_key time
        time_type string
        time_format "%Y-%m-%dT%H:%M:%S.%NZ"
        keep_time_key false
      </parse>
    </source>
    
    # enrich with kubernetes metadata
    <filter kubernetes.**>
      @type kubernetes_metadata
    </filter>

    <filter kubernetes.**>
      @type grep

      <or>
        <regexp>
          key $.kubernetes.labels.k8s-app
          pattern /^jool$/
        </regexp>
        <regexp>
          key $.kubernetes.namespace_name
          pattern /^jhub|jhub-users$/
        </regexp>
        <regexp>
          key $.kubernetes.labels.app
          pattern /^cert-manager|gpushare|nginx-ingress|postgresql$/
        </regexp>
        <regexp>
          key $.kubernetes.labels.tier
          pattern /^control-plane$/
        </regexp>
      </or>
    </filter>

    # <match **>
    #   @type stdout
    # </match>

    # Forward all logs to the aggregators
    <match **>
      @type forward
      <server>
        host fluentd-0.fluentd-headless.kube-logging.svc.cluster.local
        port 24224
      </server>

      <buffer>
        @type file
        path /opt/bitnami/fluentd/logs/buffers/logs.buffer
        flush_thread_count 2
        flush_interval 5s
      </buffer>
    </match>
