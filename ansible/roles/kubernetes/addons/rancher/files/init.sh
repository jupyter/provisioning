#!/bin/bash

set -e

if [ -z "${RANCHER_SERVERURL}" ]; then
    echo "Missing envvar: RANCHER_SERVERURL"
    exit 1
fi

if [ -z "${RANCHER_PASSWORD}" ]; then
    echo "Missing envvar: RANCHER_PASSWORD"
    exit 1
fi

RANCHER_API=${RANCHER_API:-${RANCHER_SERVERURL}/v3}

RANCHER_CLUSTERNAME=local

# Wait until Rancher is ready
while true; do
  curl -sLk "${RANCHER_SERVERURL}/ping" && break
  sleep 2
done

# Login token good for 1 minute
echo "Get login token...."
while true; do
    LOGINTOKEN=$(curl -k -s "${RANCHER_API}-public/localProviders/local?action=login" -H "Content-Type: application/json" --data-binary "{ \"username\": \"admin\", \"password\": \"initial-password-does-not-matter\", \"ttl\": 60000 }" | jq -r .token)

    [ -n "${LOGINTOKEN}" ] && break
    sleep 2
done

# Test if cluster is created
echo "Check cluster id"
while true; do
    curl -sLk -H "Authorization: Bearer ${LOGINTOKEN}" "${RANCHER_API}/clusters?name=${RACHER_CLUSTERNAME}" | jq .
    CLUSTERID=$(curl -sLk -H "Authorization: Bearer ${LOGINTOKEN}" "${RANCHER_API}/clusters?name=${RACHER_CLUSTERNAME}" | jq -r '.data[].id')

    [ -n "${CLUSTERID}" ] && break
    sleep 2
done

# Change password
echo "Change Admin password"
curl -k -s "${RANCHER_API}/users?action=changepassword" \
    -H "Content-Type: application/json" \
    -H "Authorization: Bearer ${LOGINTOKEN}" \
    -X GET --data-binary "{ \"currentPassword\": \"admin\", \"newPassword\": \"${RANCHER_PASSWORD}\" }"

# Create API key good forever
APIKEY=$(curl -k -s "${RANCHER_API}/token" -H "Content-Type: application/json" -H "Authorization: Bearer ${LOGINTOKEN}" --data-binary "{ \"type\": \"token\", \"description\": \"for init script\" }" | jq -r .token)

echo "API Key: ${APIKEY}"

# Set server-url
echo "Set Server URL"
curl -k -s "${RANCHER_API}/settings/server-url" \
    -H "Content-Type: application/json" \
    -H "Authorization: Bearer ${APIKEY}" \
    -X PUT --data-binary "{ \"name\": \"server-url\", \"value\": \"${RANCHER_SERVERURL}\" }"

# Do whatever else you want
