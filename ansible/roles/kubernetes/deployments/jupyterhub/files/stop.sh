#!/bin/sh
#
# Stop script for single-user pods
# 
# This script is executed via a Kubernetes preStop pod lifecycle hook
# We pass some env vars via kubespawner
#
# Author: Steffen Vogel <svogel2@eonerc.rwth-aachen.de>

#
# We might do some offsite backup here later...
#