async def update_affiliations(user, authenticator, handler, authentication):
    auth_state = authentication.get('auth_state')

    affiliations = auth_state.get('Edupersonscopedaffiliation')
    affiliations = affiliations.split(';')

    for affiliation in affiliations:
            group_name = f'affiliation-{affiliation}'

            group = ensure_group(authenticator.db, group_name, authenticator.log)

            if user.orm_user not in group.users:
                group.users.append(user.orm_user)

    authenticator.db.commit()

    return authentication

