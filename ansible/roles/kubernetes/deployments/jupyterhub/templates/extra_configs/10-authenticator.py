c.RemoteUserAuthenticator.auth_state_header_names = [
    'Rwthsystemids',
    'Rwthmailaddress',
    'Rwthdienstemail',
    'Persistent-Id',
    'Persistent-Hash-Mixed-Case',
    'Persistent-Hash',
    'Mail',
    'Entitlement',
    'Edupersonscopedaffiliation',
    'Edupersonentitlement',
    'Affiliation',
    'Shib-Session-Index',
    'Shib-Session-Inactivity',
    'Shib-Session-Id',
    'Shib-Session-Expires',
    'Shib-Identity-Provider',
    'Shib-Handler',
    'Shib-Authncontext-Class',
    'Shib-Authentication-Method',
    'Shib-Authentication-Instant',
    'Shib-Application-Id',
    'Auth_type'
]

c.RemoteUserAuthenticator.header_name = 'Persistent-Hash'
