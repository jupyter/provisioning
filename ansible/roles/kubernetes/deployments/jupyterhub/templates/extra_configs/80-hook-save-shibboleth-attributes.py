header_names = [
    'Rwthsystemids',
    'Rwthmailaddress',
    'Rwthdienstemail',
    'Persistent-Id',
    'Persistent-Hash-Mixed-Case',
    'Persistent-Hash',
    'Mail',
    'Entitlement',
    'Edupersonscopedaffiliation',
    'Edupersonentitlement',
    'Affiliation',
    'Shib-Session-Index',
    'Shib-Session-Inactivity',
    'Shib-Session-Id',
    'Shib-Session-Expires',
    'Shib-Identity-Provider',
    'Shib-Handler',
    'Shib-Authncontext-Class',
    'Shib-Authentication-Method',
    'Shib-Authentication-Instant',
    'Shib-Application-Id',
    'Auth_type'
]

async def save_shibboleth_attributes(user, authenticator, handler, authentication):
    auth_state = authentication.get('auth_state')

    for header_name in header_names:
        header_value = handler.request.headers.get(header_name)
        if header_value:
            auth_state[header_name] = header_value

    return authentication

