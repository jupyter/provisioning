import logging
from jupyterhub import orm

def ensure_group(db, name, logger):
    group = orm.Group.find(db, name=name)
    if not group:
        # create the group
        logger.info('Creating new group %s', name)
        group = orm.Group(name=name)
        db.add(group)

    return group

