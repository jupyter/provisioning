c.KubeSpawner.http_timeout = 5 * 60
c.KubeSpawner.start_timeout = 10 * 60
c.KubeSpawner.image_pull_policy = 'Always'
c.KubeSpawner.ip = '::'

c.KubeSpawner.namespace = 'jhub-users'
c.KubeSpawner.image_pull_secrets = 'registry-jupyter-rwth-aachen-de'

# Use user id instead of name for Kubernetes resources
c.KubeSpawner.pod_name_template = 'jupyter-{userid}'

c.KubeSpawner.lifecycle_hooks = {
  'postStart': {
    'exec': { 'command': [ '/bin/bash', '/scripts/start.sh' ] }
  },
  'preStop': {
    'exec': { 'command': [ '/bin/bash', '/scripts/stop.sh' ] }
  }
}
