from kubernetes import client
from kubespawner.utils import get_k8s_model
from kubernetes.client.models import ( V1Volume, V1VolumeMount )

admin = {
  'name': 'admin',
  'pvc': 'jhub-admin',
  'mountPath': '/home/jovyan/admin',
  'readOnly': False
}

sciebo = {
  'name': 'sciebo',
  'pvc': 'sciebo-3dpt00-rwth-aachen-de',
  'mountPath': '/home/jovyan/sciebo',
  'readOnly': False
}

user_volume_map = {
  'yit24flz': [ admin ], # Marcus Meyer
  'vzi3jsam': [ admin ], # Steffen Vogel
}

def add_extra_volumes(spawner, pod):
    try:
        user = spawner.user

        # Add additional shared volumes for user
        if user.name in user_volume_map:
            for volume in user_volume_map[user.name]:
                pod.spec.volumes.append(
                    get_k8s_model(V1Volume, {
                        'name' : volume['name'],
                        'persistentVolumeClaim': {
                            'claimName': volume['pvc']
                        }
                    })
                )

                # Note implicitly only 1 container...
                pod.spec.containers[0].volume_mounts.append(
                    get_k8s_model(V1VolumeMount, {
                        'name' : volume['name'],
                        'mountPath' : volume['mountPath'],
                        'readOnly': volume.get('readOnly', False)
                    })
                )

        # Give admin users R/W access to dataset and materials mounts
        if user.admin:
            for vm in pod.spec.containers[0].volume_mounts:
                if vm.name in ['datasets', 'materials']:
                    vm.read_only = False

    except Exception as e:
        spawner.log.info("Exception in shared-mounts: " + str(e))
        pass

    return pod
