import os

for svc in {{ jupyterhub_services | repr }}:
    c.JupyterHub.services.append({
        'name': svc,
        'url': f'http://service-{svc}',
        'oauth_client_id': svc,
        'oauth_redirect_uri': f'/services/{svc}/oauth_callback',
        'oauth_no_confirm': True,
        'admin': True,
        'api_token': os.environ['JUPYTERHUB_TOKEN_SERVICE_' + svc.upper()]
    })
c.JupyterHub.services.append({
    'name':  'course101',
    'url': 'http://service-course101',
    'command': ['jupyterhub-singleuser', '--debug'],
})
