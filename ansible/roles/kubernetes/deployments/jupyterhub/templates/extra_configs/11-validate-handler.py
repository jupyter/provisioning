import json
from jupyterhub.apihandlers.base import APIHandler
from jupyterhub import orm
from tornado import web

class ValidationHandler(APIHandler):
    async def get(self):
        user = await self.get_current_user()
        if user is None:
            user = self.get_current_user_oauth_token()
        if user is None:
            raise web.HTTPError(403)

        auth_state = await user.get_auth_state()
        print(auth_state)

        model = {
            'name': user.name,
            'preferred_username': user.name,
            'groups': [g.name for g in user.groups],
            'email': auth_state.get('Mail', '')
        }

        if user.admin:
            model['groups'].append('admin')

        self.write(json.dumps(model))

c.JupyterHub.extra_handlers.append(("/api/oauth2/validate", ValidationHandler))
