import logging

class PermissionDenied(Exception):
    pass

def exception_handler(func):
    async def inner(*args, **kwargs):
        try:
            return await func(*args, **kwargs)

        # permission denied exceptions will continue to bubble up
        # until spawn handler will abort the launch
        except PermissionDenied:
            raise
        except:
            logger = args[0].log
            logger.exception('Exception in ' + func.__name__)

    return inner


def modify_pod_hook(spawner, pod):
    pod = add_extra_volumes(spawner, pod)

    return pod


async def post_auth_hook(authenticator, handler, authentication):
    username = authentication.get('name')
    user = handler.user_from_username(username)

    if authentication['auth_state'] is None:
        authentication['auth_state'] = {}

    authentication = await save_shibboleth_attributes(user, authenticator, handler, authentication)
    authentication = await update_affiliations(user, authenticator, handler, authentication)
    authentication = await update_moodle_roles(user, authenticator, handler, authentication)

    return authentication


async def pre_spawn_hook(spawner):
    try:
        check_permission(spawner)
    except PermissionDenied:
        logging.warn('Permission denied: user=%s, profile=%s', spawner.user.name, spawner.user_options.get('slug', '-'))
        raise


c.Spawner.pre_spawn_hook = pre_spawn_hook
c.KubeSpawner.modify_pod_hook = modify_pod_hook
c.Authenticator.post_auth_hook = post_auth_hook
