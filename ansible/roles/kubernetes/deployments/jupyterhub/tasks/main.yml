---

- name: Create Namespaces
  k8s:
    name: "{{ item }}"
    kind: Namespace
  loop:
  - jhub
  - jhub-users

- name: Setup RBAC for hub access to jhub-user namespace
  k8s:
    definition: "{{ lookup('file', 'rbac.yaml') }}"

- name: Create shared volumes for Jupyter materials
  k8s:
    wait: yes
    definition: "{{ lookup('file', item) }}"
  loop:
  - pvc-jhub-materials.yaml
  - pvc-jhub-datasets.yaml
  - pvc-jhub-shared.yaml
  - pvc-jhub-admin.yaml

- name: Delete previous job
  k8s:
    kind: Job
    name: materials-updater
    state: absent
    namespace: jhub-users

- name: Create job to download materials and datasets
  k8s:
    definition: "{{ lookup('template', 'job-materials-updater.yaml') }}"

- name: Create ServiceMonitor for hub
  k8s:
    definition: "{{ lookup('file', 'service-monitor.yaml') }}"

- name: Create ConfigMap for singleuser scripts
  k8s:
    definition:
      apiVersion: v1
      kind: ConfigMap
      metadata:
        name: singleuser-scripts
        namespace: jhub-users
      data:
        start.sh: |
          {{ lookup('file', 'start.sh') }}
        stop.sh: |
          {{ lookup('file', 'stop.sh') }}

- name: Check if service tokens exist in password-store
  stat:
    path: "~/.password-store/jupyter/service-token/{{ item }}.gpg"
  register: stat_result
  loop: "{{ jupyterhub_services }}"

- name: Create service tokens if they dont exist yet
  shell: "pass generate jupyter/service-token/{{ item.item }} 64 -n"
  loop: "{{ stat_result.results }}"
  when: not item.stat.exists

- name: Deploy JupyterHub services for RWTH
  k8s:
    definition: "{{ lookup('template', 'services.yaml.j2', template_vars=dict(service=item)) }}"
  loop: "{{ jupyterhub_services }}"

- name: Prepare values.yaml
  template:
    src: values.yaml.j2
    dest: /tmp/jupyterhub.yaml

- name: Install the Zero-to-JupyterHub Helm chart
  command: helm upgrade jupyterhub /home/jupyter/zero-to-jupyterhub-k8s/jupyterhub/
    --namespace jhub
    --values /tmp/jupyterhub.yaml
    --install
    --wait
