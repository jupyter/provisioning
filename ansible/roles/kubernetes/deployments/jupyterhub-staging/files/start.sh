#!/bin/sh
#
# Start-up script for single-user pods
#
# This script is executed via a Kubernetes postStart pod lifecycle hook
# We pass some env vars via kubespawner
#
# Author: Steffen Vogel <svogel2@eonerc.rwth-aachen.de>

# Set Git user settings
if command -v git >/dev/null; then
    if ! git config user.name; then
        git config --global user.name "JupyterHub User"
        git config --global user.email ${JUPYTERHUB_USER}@jupyter.rwth-aachen.de
    fi
fi

# Pull repo
if command -v gitpuller >/dev/null; then
    if [ -n "${JUPYTERHUB_REPO_SSH_ID}" ]; then
        SSH_IDENTITY_FILE=$(mktemp)
        base64 -d <<< ${JUPYTERHUB_REPO_SSH_ID} > ${SSH_IDENTITY_FILE}
        echo >> ${SSH_IDENTITY_FILE} # append newline
        chmod 600 ${SSH_IDENTITY_FILE}
        export GIT_SSH_COMMAND="ssh -i ${SSH_IDENTITY_FILE} -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"
    fi

    if [ -n "${JUPYTERHUB_REPO_PULL}" ]; then
        if [ "${JUPYTERHUB_REPO_PULL}" == "true" ]; then
            git -C ${JUPYTERHUB_PROFILE} remote set-url origin ${JUPYTERHUB_REPO_URL}

            JUPYTERHUB_REPO_REF=${JUPYTERHUB_REPO_REF:-master}

            gitpuller ${JUPYTERHUB_REPO_URL} ${JUPYTERHUB_REPO_REF} ${JUPYTERHUB_PROFILE} || true
        fi
    fi

    if [ -n "${IDENTITY_FILE}" ]; then
        rm ${SSH_IDENTITY_FILE}
    fi
fi

echo "Started singleuser container: profile=${JUPYTERHUB_PROFILE} user=${JUPYTERHUB_USER}"
