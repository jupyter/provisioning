import os

for svc in {{ jupyterhub_services | repr }}:
    c.JupyterHub.services.append({
        'name': svc,
        'url': f'http://service-{svc}',
        'oauth_client_id': svc,
        'oauth_redirect_uri': f'/services/{svc}/oauth_callback',
        'oauth_no_confirm': True,
        'admin': True,
        'api_token': os.environ['JUPYTERHUB_TOKEN_SERVICE_' + svc.upper()]
    })
# ngshare
c.JupyterHub.services.append({
   'name': 'ngshare',
   'url': 'http://ngshare.jhub-staging.svc.cluster.local',
   'api_token': '{{ lookup("passwordstore", "jupyter-staging/service-token/ngshare") }}',
   'oauth_no_confirm': True,
   'oauth_client_id': 'ngshare'
})

# c.JupyterHub.services.append(
#     {
#         'name': 'ngshare',
#         'url': 'http://staging.jupyter.rwth-aachen.de/services/ngshare',
#         'command': ['python3', '-m', 'ngshare', '--admins', 'Uq1r5F7G3YNiAa4E8GynoDOpuZNu6K9frMRDF4cUI3qRSPvZvgAOO9nrLK6f2LKr@rwth-aachen.de'],
#     }
# )
