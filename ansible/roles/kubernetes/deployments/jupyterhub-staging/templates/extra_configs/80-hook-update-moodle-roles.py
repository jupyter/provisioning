import aiohttp
import re

token = os.environ.get('MOODLE_TOKEN')
host = 'moodle.rwth-aachen.de'
function = 'local_rwth_webservices_get_courses_for_lmsid'
url = f'https://{host}/webservice/rest/server.php'


def get_lmsid(auth_state):
    rwth_ids = auth_state.get('Rwthsystemids')

    m = re.search('name=lms-id:value=([a-z0-9_]+)', rwth_ids)
    if m:
        return m.group(1)

async def fetch_moodle_roles(lmsid):

    timeout = aiohttp.ClientTimeout(total=10)

    params = {
        'lmsid': lmsid,
        'wstoken': token,
        'wsfunction': function,
        'moodlewsrestformat': 'json'
    }

    async with aiohttp.ClientSession(
        raise_for_status=True,
        timeout=timeout) as session:

        async with session.get(url, params=params) as response:
            return await response.json()

async def update_moodle_roles(user, authenticator, handler, authentication):
    auth_state = authentication.get('auth_state')
    lmsid = get_lmsid(auth_state)

    # TODO: revoke group membership in Jupyter if user has been removed from Moodle course

    if lmsid:
        courses = await fetch_moodle_roles(lmsid)
        for course in courses:
            course_id = course['courseid']
            course_roles = course['roles']

            # remove duplicates
            course_roles = list(set(course_roles))

            for course_role in course_roles:
                group_name = f'moodle-{course_role}@{course_id}'

                group = ensure_group(authenticator.db, group_name, authenticator.log)

                if user.orm_user not in group.users:
                    group.users.append(user.orm_user)

        authenticator.db.commit()

    return authentication

