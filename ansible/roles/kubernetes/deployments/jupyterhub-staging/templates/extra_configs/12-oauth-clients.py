c.JupyterHub.services.append({
    'name': 'slew',
    'admin': False,
    'api_token': '{{ lookup('passwordstore', 'jupyter/oauth-slew') }}',
    'oauth_client_id': 'slew',
    'oauth_no_confirm': False,
    'oauth_redirect_uri': 'https://staging.slew.rwth-aachen.de/oauth2/callback'
})
