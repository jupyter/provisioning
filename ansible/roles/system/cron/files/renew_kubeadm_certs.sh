#!/bin/bash


EXPIRE=$(date --date="$(openssl x509 -in /etc/kubernetes/pki/apiserver.crt -noout -enddate | cut -d= -f2)" "+%s")
NOW=$(date "+%s")

REMAINING=$((${EXPIRE} - ${NOW}))

echo "Expires in ${REMAINING} seconds"

if (( ${REMAINING} < $((60*60*24)) )); then
        echo "Renewing..."
        kubeadm alpha certs renew all
else
        echo "Certs still valid for more than 1h. Not renewing"
fi
