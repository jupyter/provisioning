require 'json'

module Ansible

    def Ansible.get_hostvars(node)
        host = node.metadata['ansible_inventory_hostname']

        hostvars_raw = %x{ansible -m debug #{host} -a 'var=hostvars[inventory_hostname]' | sed 's#.*SUCCESS =>##'}
        
        JSON.parse(hostvars_raw)['hostvars[inventory_hostname]']
    end

end
