# RWTHjuptyer Provisioning Repo

This repo contains the full configuration and tooling for provision RWTHjupyter.
We release this to the public as a guideline and example on how other large-scale JupyterHub deployements can look like.

The progvisioning is separated into two phases:

- `razor/`: The Razor Server task for baremetal provisioning of the cluster nodes to a fresh CentOS
- `ansible/`: The Ansible roles and playbooks to setup the cluster from a fresh CentOS

See: https://jupyter.pages.rwth-aachen.de/documentation-admin/Provisioning.html

## Authors

- Steffen Vogel <svogel2@eonerc.rwth-aachen.de> (Institute for Automation of Complex Power Systems, RWTH Aachen)
- Markus Meyer <mmeyer@itc.rwth-aachen.de> (IT Center, RWTH Aachen)
- Jonas Jansen <jansen@itc.rwth-aachen.de> (IT Center, RWTH Aachen)
