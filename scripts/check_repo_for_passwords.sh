#!/bin/bash

BASE_DIR=${HOME}

PASSWORD_HEAD=$(git -C ${BASE_DIR}/passwords rev-parse HEAD)

COMMITS=$(git -C ${BASE_DIR}/passwords log --oneline | cut -f1 -d" ")

for COMMIT in ${COMMITS}; do
	git -C ${BASE_DIR}/passwords checkout ${COMMIT}

	PASSWORDS=$(find ${BASE_DIR}/passwords -name "*.gpg")

	for PASSWORD in ${PASSWORDS}; do
		DECRYPTED_PASSWORD=$(gpg -d < ${PASSWORD} | head -n1)
		echo "${PASSWORD} ${DECRYPTED_PASSWORD}" >> passwords.txt
	done

	cat passwords.txt passwords-all.txt | sort -u > passwords-all-unique.txt
	mv passwords-all-unique.txt passwords-all.txt
	rm passwords.txt
done

git -C ${BASE_DIR}/passwords checkout ${PASSWORD_HEAD}

while read LINE; do
	NAME=$(echo $LINE | cut -f1 -d" ")
	PASSWORD=$(echo $LINE | cut -f2 -d" ")

	echo "Password: ${NAME} (${PASSWORD})"
	git -C provisioning log --pickaxe-all --oneline -S ${PASSWORD}
done < passwords-all.txt
