#!/bin/sh

if (( $# < 1 )); then
    echo "usage: $0 ROLE [NODES]"
    exit 1
fi

ROLE=${1}
NODES=${2:-all}

ansible -v  --module-name include_role --args name=${ROLE} ${NODES}
