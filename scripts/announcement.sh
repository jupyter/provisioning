#!/bin/bash

if [ $# != 2 ]; then
cat << EOF
Usage:
  Add new announcement or overwrite existing one:
     $0 add "This is my announcement"

  Delete current announcement:
    $0 delete

Hint: You can use HTML and FontAwesome Icons
  https://fontawesome.com/

Example:
  $0 add "<i aria-hidden='true' class='fa fa-birthday-cake'></i> Welcome to the new JupyterHub Cluster! We are now running on a new hardware! Old home directories have <b>not</b> been migrated. <i aria-hidden='true' class='fa fa-exclamation-triangle'></i>"

EOF
exit -1
fi

CMD="$1"
TEXT="$2"

URL=https://jupyter.rwth-aachen.de/services/announcement/
TOKEN=$(pass jupyter/hub_admin_token | head -n1)

case $CMD in
	add)
		curl -k -v -X POST -d "{ \"announcement\": \"${TEXT}\" }" -H "Authorization: token ${TOKEN}" ${URL}
		;;

	delete)
		curl -k -v -X DELETE -H "Authorization: token ${TOKEN}" ${URL}
		;;
esac
