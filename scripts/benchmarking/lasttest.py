import argparse
import logging
import requests
import json
import time

logging.basicConfig(level=logging.INFO)

def sort_list(list, key):
  temp_list = list
  output_list = []
  while(temp_list):
    temp = ''
    for entry in temp_list:
      if not entry[key] is None:
        if temp == '' or temp[key] > entry[key]:
          temp = entry
      else:
        temp_list.remove(entry)
    output_list.append(temp)
    temp_list.remove(temp)
  return output_list


def get_users(api_url, headers, users_file):
  r = requests.get(api_url + '/users', headers=headers)

  json_output = r.json()

  print(json_output)

  json_output = sort_list(json_output, 'last_activity')

  print(json_output)

  with open(users_file, 'w') as fw:
    fw.write(json.dumps(json_output))


def benchmark(api_url, headers, users_file, number_of_users, sleep, mode='populate'):
  with open(users_file, 'r') as fr:
    content = fr.read()

  json_output = json.loads(content)

  json_output = json_output[:int(number_of_users)]

  for i, entry in enumerate(json_output):
    if mode == 'populate':
      r = requests.post('{}/users/{}/server'.format(api_url, entry['name']), headers=headers)
    elif mode == 'remove':
      r = requests.delete('{}/users/{}/server'.format(api_url, entry['name']), headers=headers)
    logging.info('Entry {}/{} | Status-Code: {}'.format(i + 1, len(json_output), r.status_code))
    time.sleep(int(sleep))


api_url = 'https://jupyter.rwth-aachen.de/hub/api'

time_record = []

parser = argparse.ArgumentParser(description='Benchmarking for RWTHjupyter')
parser.add_argument('-m', '--mode', dest='mode', action='store', default='populate')
parser.add_argument('-t', '--token', dest='api_token', action='store', default='')
parser.add_argument('-n','--number', dest='number', action='store', default=2)
parser.add_argument('-s', '--sleep', dest='sleep', action='store', default=2)
parser.add_argument('-o', '--output', dest='output', action='store', default='output.json')

args = parser.parse_args()

mode = args.mode
api_token = args.api_token
number_of_users = args.number
sleep = args.sleep
output = args.output

headers = {
  'Authorization': 'token %s' % api_token,
}
tic = time.perf_counter()
if mode == 'populate':
  benchmark(api_url, headers, output, number_of_users, sleep)
elif mode == 'remove':
  benchmark(api_url, headers, output, number_of_users, sleep, mode='remove')
elif mode == 'get':
  get_users(api_url, headers, output)
else:
  logging.warn('No valid mode was specified')

toc = time.perf_counter()

logging.info('Finished the execution in {} seconds'.format(toc - tic))